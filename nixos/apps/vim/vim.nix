{ pkgs, ... }:
{
 environment.variables = { EDITOR = "vim";};
 environment.systemPackages = with pkgs; [ 
 ((vim_configurable.override { python = python3; }).customize {
  name = "vim";   # Specifies the vim binary name.
  # Below you can specify what usually goes into `~/.vimrc`
  vimrcConfig.customRC = ''
    " Preferred global default settings:
    "set number                    " Enable line numbers by default
    "set background=dark           " Set the default background to dark or light
    set smartindent               " Automatically insert extra level of indentation
    set tabstop=4                 " Default tabstop
    set shiftwidth=4              " Default indent spacing
    set expandtab                 " Expand [TABS] to spaces
    syntax enable                 " Enable syntax highlighting
    set t_Co=256                  " use 265 colors in vim
    "set spell spelllang=en_au     " Default spell checking language
    "hi clear SpellBad             " Clear any unwanted default settings
    "hi SpellBad cterm=underline   " Set the spell checking highlight style
    "hi SpellBad ctermbg=NONE      " Set the spell checking highlight background
    "match ErrorMsg '\s\+$'        "
    
    " My old setting
    inoremap jk <ESC>
    set hlsearch
    set noswapfile
    set ignorecase
    set hidden
    set foldmethod=indent
    set foldlevel=99
    set incsearch
    set scrolloff=8
    set signcolumn=no
    nmap <space> za
    nmap <F2> :NERDTreeToggle<CR>
    nnoremap<F5> :buffers<CR>:buffer<space>
    set number relativenumber
    set mouse=a
    set ttymouse=sgr

    map <C-s> :FZF
    vnoremap <C-y> "+y
    vnoremap <C-p> "+p
    :augroup numbertoggle
    :  autocmd!
    :  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
    :  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
    :augroup END

    " Tab completion 
    function! Tab_Or_Complete()
    if col('.')>1 && strpart( getline('.'), col('.')-2, 3 ) =~ '^\w'
      return "\<C-N>"
    else
      return "\<Tab>"
    endif
    endfunction
    :inoremap <Tab> <C-R>=Tab_Or_Complete()<CR>
    
    let g:airline_powerline_fonts = 1   " Use powerline fonts
    let g:airline_theme='solarized'     " Set the airline theme

    set laststatus=2   " Set up the status line so it's coloured and always on

    " Add more settings below
  '';
  # store your plugins in Vim packages
  vimrcConfig.packages.myVimPackage = with pkgs.vimPlugins; {
    start = [               # Plugins loaded on launch
      airline               # Lean & mean status/tabline for vim that's light as air
      vim-airline-themes    # Collection of themes for airlin
      vim-nix               # Support for writing Nix expressions in vim
      nerdtree               # Support for writing Nix expressions in vim
      fzf-vim
      haskell-vim
      LanguageClient-neovim
      syntastic
      awesome-vim-colorschemes
    ];
    # manually loadable by calling `:packadd $plugin-name`
    # opt = [];
    # To automatically load a plugin when opening a filetype, add vimrc lines like:
    # autocmd FileType php :packadd phpCompletion
  };
})];
}
